variable "yc_token" {
  default = ""
}

variable "hosts_ssh_public_key" {
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCo+48Wll4DoPRqXqJLawYCO7lKqcNa7dIHzSCmCv+PG3ICSuzaJREI09903zlmBboQ9V5gO4zS8KT5SvtrWvQAk3+IPDow8YY0s13OcTSjUpHDbYHV+KnBZWT2Rvv8j/nd+wsK/vN6Czw1EPeQ+a4e7coTwbovnBG06uCk6T1zS9rYCXdZd7ykGCZs7fxsrlKYORXC7EUTCEblGy7ybVXCy6H6GTI4wipuLdqDCjE8ELbn2iQiewjQCFXNzjt/3lFxav/Zf8emCc1hhuKpTMt+ltCFJIQw7IcyB1ZKkEFC4AUt90CNnNaqDulkJtg5gnGfWKZ5L/6kLUtRM9L1fhxlmB5+kAplmIGrIxO7PYYxk4QOH6iCb85NQlgNZ8/otnJGQkrs37s3Gf7r95NRV+1sjNdndyPoHEZgbgXhOZl1CSguMofjDRqnfG0BBVqTSM3a88STC+9vAL1Jso2LlsOB4SxOn9VfX4DmOfibi1TKb9QkeDNVjMLBCwyYOGLpTE0="
}

variable "yc_main_service_account_id" {
  default = "aje2q9lfakd24000qafp"
}

variable "yc_cloud_id" {
  default = "b1gsgm9sgeu33apcbo2u"
}

variable "yc_folder_id" {
  default = "b1gft0uc6bptguk1tekv"
}

variable "ubuntu2204_image_id" {
  default = "fd8k3a6rj9okseiqrl3k" # Ubuntu 22.04 LTS
}