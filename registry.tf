resource "yandex_container_registry" "myappreg" {
  name = "myappreg"
  folder_id = var.yc_folder_id
  labels = {
    my-label = "myapp"
  }
}