terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"

#  backend "s3" {
#      endpoint   = "storage.yandexcloud.net"
#      bucket     = "buckettf"
#      region     = "ru-central1"
#      key        = "tfstate/backet.tfstate"


#      access_key = "xxx"
#      secret_key = "xxx"

#      skip_region_validation      = true
#      skip_credentials_validation = true
#  }


}

provider "yandex" {
  token     = var.yc_token
  cloud_id  = var.yc_cloud_id
  folder_id = var.yc_folder_id
}

