# Основная сеть
resource "yandex_vpc_network" "network-diploma" {
  name = "${terraform.workspace}-network-diploma"
}

# Публичные подсети по зонам доступа
resource "yandex_vpc_subnet" "public-a" {
  name           = "${terraform.workspace}-public-a"
  v4_cidr_blocks = ["192.168.10.0/24"]
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network-diploma.id
}

resource "yandex_vpc_subnet" "public-b" {
  name           = "${terraform.workspace}-public-b"
  v4_cidr_blocks = ["192.168.20.0/24"]
  zone           = "ru-central1-b"
  network_id     = yandex_vpc_network.network-diploma.id
}

resource "yandex_vpc_subnet" "public-c" {
  name           = "${terraform.workspace}-public-c"
  v4_cidr_blocks = ["192.168.30.0/24"]
  zone           = "ru-central1-c"
  network_id     = yandex_vpc_network.network-diploma.id
}

# Приватные сети по зонам доступа
resource "yandex_vpc_subnet" "private-a" {
  name           = "${terraform.workspace}-private-a"
  v4_cidr_blocks = ["172.16.10.0/24"]
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network-diploma.id
}

resource "yandex_vpc_subnet" "private-b" {
  name           = "${terraform.workspace}-private-b"
  v4_cidr_blocks = ["172.16.20.0/24"]
  zone           = "ru-central1-b"
  network_id     = yandex_vpc_network.network-diploma.id
}

resource "yandex_vpc_subnet" "private-c" {
  name           = "${terraform.workspace}-private-c"
  v4_cidr_blocks = ["172.16.30.0/24"]
  zone           = "ru-central1-c"
  network_id     = yandex_vpc_network.network-diploma.id
}