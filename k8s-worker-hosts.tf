# Зависимость параметров от workspace
locals {
  k8s_worker_instances_count_map = {
    stage = 3,
    prod  = 3
  }
  k8s_worker_zone_map = {
    stage = tolist([
      "ru-central1-a",
      "ru-central1-b",
      "ru-central1-c",
    ])
    prod = tolist([
      "ru-central1-a",
      "ru-central1-b",
      "ru-central1-c"
    ])
  }
  k8s_worker_subnetworks_map = {
    stage = tolist([
      yandex_vpc_subnet.public-a.id,
      yandex_vpc_subnet.public-b.id,
      yandex_vpc_subnet.public-c.id,
    ])
    prod = tolist([
      yandex_vpc_subnet.public-a.id,
      yandex_vpc_subnet.public-b.id,
      yandex_vpc_subnet.public-c.id
    ])
  }

  k8s_worker_disk_size_map = {
    stage = 60
    prod  = 100
  }
  k8s_worker_disk_type_map = {
    stage = "network-hdd"
    prod  = "network-ssd"
  }
  ks8_worker_platform_id_map = {
    stage = "standard-v1"
    prod  = "standard-v3"
  }
  ks8_worker_core_fraction_map = {
    stage = 20
    prod  = 50
  }
  k8s_worker_cores_map = {
    stage = 4
    prod  = 4
  }
  k8s_worker_ram_map = {
    stage = 4
    prod  = 8
  }
  k8s_worker_preemptible_map = {
    stage = false,
    prod  = false
  }
}

# Хосты для воркер-нод кластера K8S
resource "yandex_compute_instance" "vp_k8s_worker_nodes" {
  name     = "${terraform.workspace}-k8s-worker-${count.index}"
  hostname = "${terraform.workspace}-k8s-worker-${count.index}"
  zone     = local.k8s_worker_zone_map[terraform.workspace][count.index]

  count = local.k8s_worker_instances_count_map[terraform.workspace]

  platform_id = local.ks8_worker_platform_id_map[terraform.workspace]

  resources {
    cores         = local.k8s_worker_cores_map[terraform.workspace]
    memory        = local.k8s_worker_ram_map[terraform.workspace]
    core_fraction = local.ks8_worker_core_fraction_map[terraform.workspace]
  }

  boot_disk {
    initialize_params {
      image_id = var.ubuntu2204_image_id
      size     = local.k8s_worker_disk_size_map[terraform.workspace]
      type     = local.k8s_worker_disk_type_map[terraform.workspace]
    }
  }

  network_interface {
    subnet_id = local.k8s_worker_subnetworks_map[terraform.workspace][count.index]
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${var.hosts_ssh_public_key}"
  }

  allow_stopping_for_update = true
  scheduling_policy {
    preemptible = local.k8s_worker_preemptible_map[terraform.workspace]
  }
}
