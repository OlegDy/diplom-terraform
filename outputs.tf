
########################################################################################################################
# k8s master - ноды
output "k8s_master_nodes_ips" {
  value      = [yandex_compute_instance.vp_k8s_master_nodes[*].network_interface[0].nat_ip_address]
  depends_on = [
    yandex_compute_instance.vp_k8s_master_nodes
  ]
}
output "k8s_master_nodes_internal_ips" {
  value      = [yandex_compute_instance.vp_k8s_master_nodes[*].network_interface[0].ip_address]
  depends_on = [
    yandex_compute_instance.vp_k8s_master_nodes
  ]
}
########################################################################################################################
# k8s worker - ноды
output "k8s_worker_nodes_ips" {
  value      = [yandex_compute_instance.vp_k8s_worker_nodes[*].network_interface[0].nat_ip_address]
  depends_on = [
    yandex_compute_instance.vp_k8s_worker_nodes
  ]
}
output "k8s_worker_nodes_internal_ips" {
  value      = [yandex_compute_instance.vp_k8s_worker_nodes[*].network_interface[0].ip_address]
  depends_on = [
    yandex_compute_instance.vp_k8s_worker_nodes
  ]
}


