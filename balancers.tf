

######################################################################################################################
# Целевая группа для балансировщика характеристик мониторинга
resource "yandex_lb_target_group" "nlb-target-group-diploma-monitoring" {
  name      = "${terraform.workspace}-nlb-target-group-diploma-monitoring"
  region_id = "ru-central1"

  target {
    address   = yandex_compute_instance.vp_k8s_master_nodes.0.network_interface.0.ip_address
    subnet_id = yandex_compute_instance.vp_k8s_master_nodes.0.network_interface.0.subnet_id
  }


  depends_on = [
    yandex_compute_instance.vp_k8s_master_nodes,
    yandex_vpc_subnet.public-a,
    yandex_vpc_subnet.public-b,
    yandex_vpc_subnet.public-c,
    yandex_vpc_network.network-diploma
  ]
}

# Балансировщик для доступа к характеристикам мониторинга
resource "yandex_lb_network_load_balancer" "nlb-diploma-monitoring" {
  name = "${terraform.workspace}-nlb-diploma-monitoring"
  type = "external"

  # listeners для компонентов мониторинга кластера
  # alertmanager
  listener {
    name        = "${terraform.workspace}-nlb-listener-diploma-alert-manager"
    port        = 9093
    target_port = 30903
    protocol    = "tcp"
    external_address_spec {
      ip_version = "ipv4"
    }
  }
  # graphana
  listener {
    name        = "${terraform.workspace}-nlb-listener-diploma-graphana"
    port        = 8080
    target_port = 30680
    protocol    = "tcp"
    external_address_spec {
      ip_version = "ipv4"
    }
  }
  # prometheus
  listener {
    name        = "${terraform.workspace}-nlb-listener-diploma-prometheus"
    port        = 9090
    target_port = 30090
    protocol    = "tcp"
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = "${yandex_lb_target_group.nlb-target-group-diploma-monitoring.id}"

    healthcheck {
      name = "tcp"
      tcp_options {
        port = 30680
      }
    }
  }

  depends_on = [
    yandex_lb_target_group.nlb-target-group-diploma-monitoring,
    yandex_compute_instance.vp_k8s_master_nodes,
    yandex_vpc_subnet.public-a,
    yandex_vpc_subnet.public-b,
    yandex_vpc_subnet.public-c,
    yandex_vpc_network.network-diploma
  ]
}

########################################################################################################################
# Балансировщик для доступа к приложению в кластере
resource "yandex_lb_network_load_balancer" "nlb-diploma-webapp" {
  name = "${terraform.workspace}-nlb-diploma-webapp"
  type = "external"

  # listener для приложения
  listener {
    name        = "${terraform.workspace}-nlb-listener-diploma-webapp"
    port        = 80
    target_port = 30080
    protocol    = "tcp"
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = "${yandex_lb_target_group.nlb-target-group-diploma-webapp.id}"

    healthcheck {
      name = "tcp"
      tcp_options {
        port = 30080
      }
    }
  }

  depends_on = [
    yandex_lb_target_group.nlb-target-group-diploma-webapp,
    yandex_compute_instance.vp_k8s_worker_nodes,
    yandex_vpc_subnet.public-a,
    yandex_vpc_subnet.public-b,
    yandex_vpc_subnet.public-c,
    yandex_vpc_network.network-diploma
  ]
}

# Целевая группа для балансировщика
resource "yandex_lb_target_group" "nlb-target-group-diploma-webapp" {
  name      = "${terraform.workspace}-nlb-target-group-diploma-webapp"
  region_id = "ru-central1"

  # TODO возможно переписать это через циклы? чтобы автоматически заносились все рабочие ноды
  target {
    address   = yandex_compute_instance.vp_k8s_worker_nodes.0.network_interface.0.ip_address
    subnet_id = yandex_compute_instance.vp_k8s_worker_nodes.0.network_interface.0.subnet_id
  }

  target {
    address   = yandex_compute_instance.vp_k8s_worker_nodes.1.network_interface.0.ip_address
    subnet_id = yandex_compute_instance.vp_k8s_worker_nodes.1.network_interface.0.subnet_id
  }

  target {
    address   = yandex_compute_instance.vp_k8s_worker_nodes.2.network_interface.0.ip_address
    subnet_id = yandex_compute_instance.vp_k8s_worker_nodes.2.network_interface.0.subnet_id
  }


  depends_on = [
    yandex_compute_instance.vp_k8s_worker_nodes,
    yandex_vpc_subnet.public-a,
    yandex_vpc_subnet.public-b,
    yandex_vpc_subnet.public-c,
    yandex_vpc_network.network-diploma
  ]
}

